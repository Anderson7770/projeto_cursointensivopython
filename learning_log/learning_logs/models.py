from django.db import models

# Create your models here.
class Topic(models.Model):
    """Um assunto sobre o qual o usuario esteja aprendendo!"""
    text = models.CharField(max_length=200)
    data_added = models.DateTimeField(auto_now_add=True)


class Entry_1(models.Model):
    """Algo especifico aprendido sobre um assunto."""
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "entries"


    def __str__(self):
        """Devolve uma representação em string do modelo"""
        if self.text >= 50:
            return self.text[:50] + "..."

        return self.text
        #return self.text[:50] + "..." 
        

# PROXIMO PROJETO
#18.4 – Pizzaria: Inicie um novo projeto chamado pizzaria com uma aplicação
#chamada pizzas. Defina um modelo Pizza com um campo chamado name, que
#armazenará nomes como Hawaiian e Meat Lovers. Defina um modelo chamado
#Topping com campos de nome pizza e name. O campo pizza deve ser uma
#chave estrangeira para Pizza, e name deve ser capaz de armazenar valores como
#pineapple, Canadian bacon e sausage.
#Registre os dois modelos no site de administração e use esse site para fornecer
#alguns nomes de pizzas e de ingredientes. Utilize o shell para explorar os dados
#inseridos.